<?php

declare(strict_types=1);

namespace Deathstar\Token;

class Token
{
    
    /**
     * @var string
     */
    private $accessToken;
    
    /**
     * @var int
     */
    private $expiresIn;
    
    /**
     * @var string
     */
    private $tokenType;
    
    /**
     * @var string
     */
    private $scope;
    
    /**
     * @param string $accessToken
     * @param int    $expiresIn
     * @param string $tokenType
     * @param string $scope
     */
    public function __construct(string $accessToken, int $expiresIn, string $tokenType, string $scope)
    {
        $this->accessToken = $accessToken;
        $this->expiresIn = $expiresIn;
        $this->tokenType = $tokenType;
        $this->scope = $scope;
    }
    
    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }
    
    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }
    
    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }
    
    /**
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->tokenType;
    }
}
