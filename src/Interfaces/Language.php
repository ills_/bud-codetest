<?php

declare(strict_types=1);

namespace Deathstar\Interfaces;

interface Language
{
    
    /**
     * @return string
     */
    public function getPhrase(): string;
}
