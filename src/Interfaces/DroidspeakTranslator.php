<?php

declare(strict_types=1);

namespace Deathstar\Interfaces;

use Deathstar\Languages\Droidspeak;

interface DroidspeakTranslator
{
    
    /**
     * @param Droidspeak $droidspeak
     * @return string
     */
    public function translate(Droidspeak $droidspeak): string;
}
