<?php

declare(strict_types=1);

namespace Deathstar\Services;

use Deathstar\Client\Prisoner;
use Deathstar\Exceptions\DroidspeakException;
use Deathstar\Exceptions\PrisonerTranslationException;
use Deathstar\Interfaces\DroidspeakTranslator;
use Deathstar\Languages\Droidspeak;

class DroidspeakTranslationService
{
    
    /**
     * @var DroidspeakTranslator
     */
    private $translator;
    
    /**
     * @param DroidspeakTranslator $translator
     */
    public function __construct(DroidspeakTranslator $translator)
    {
        $this->translator = $translator;
    }
    
    /**
     * @param Prisoner $prisoner
     * @return Prisoner
     * @throws PrisonerTranslationException
     */
    public function translatePrisonerLocation(Prisoner $prisoner): Prisoner
    {
        try {
            $translatedPrisoner = new Prisoner(
                $prisoner->getPrisoner(),
                $this->translator->translate(new Droidspeak($prisoner->getCell())),
                $this->translator->translate(new Droidspeak($prisoner->getBlock()))
            );
        } catch (DroidspeakException $e) {
            throw new PrisonerTranslationException("Could not translate prisoner location.");
        }
     
        return $translatedPrisoner;
    }
}
