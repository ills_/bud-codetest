<?php

declare(strict_types=1);

namespace Deathstar\Utils;

class Str
{
    
    /**
     * Removes all whitespaces from given string
     *
     * @param string $string
     * @return string
     */
    public static function stripWhitespace(string $string): string
    {
        return preg_replace('/\s+/', '', $string);
    }
}
