<?php

declare(strict_types=1);

namespace Deathstar\Client;

class Prisoner
{
    
    /**
     * @var string
     */
    private $prisoner;
    
    /**
     * @var string
     */
    private $cell;
    
    /**
     * @var string
     */
    private $block;
    
    public function __construct(string $prisoner, string $cell, string $block)
    {
        $this->prisoner = $prisoner;
        $this->cell = $cell;
        $this->block = $block;
    }
    
    /**
     * @return string
     */
    public function getPrisoner(): string
    {
        return $this->prisoner;
    }
    
    /**
     * @return string
     */
    public function getCell(): string
    {
        return $this->cell;
    }
    
    /**
     * @return string
     */
    public function getBlock(): string
    {
        return $this->block;
    }
}
