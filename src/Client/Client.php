<?php

declare(strict_types=1);

namespace Deathstar\Client;

use Deathstar\Exceptions\InvalidResponseException;
use Deathstar\Token\Token;
use Deathstar\Exceptions\ConnectionException;
use GuzzleHttp\ClientInterface;
use Deathstar\Exceptions\ClientException as DeathstarClientException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Client
{

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     * @return Response
     * @throws ConnectionException
     * @throws DeathstarClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function makeRequest(string $method, string $uri, array $options = []): Response
    {
        try {
            $response = $this->client->request($method, $uri, $options);
        } catch (ClientException $e) {
            throw new DeathstarClientException($e->getMessage());
        } catch (RequestException $e) {
            throw new ConnectionException($e->getMessage());
        }

        return new Response(
            $response->getStatusCode(),
            $response->getBody()->getContents()
        );
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     *
     * @return Token
     * @throws ConnectionException
     * @throws DeathstarClientException
     * @throws InvalidResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authenticate(string $clientId, string $clientSecret): Token
    {
        $response = $this->makeRequest('post', 'token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            ]
        ]);
        
        $tokenData = json_decode($response->getBody());
        
        if (!isset($tokenData->access_token, $tokenData->expires_in, $tokenData->token_type, $tokenData->scope)) {
            throw new InvalidResponseException();
        }
        
        return new Token(
            (string) $tokenData->access_token,
            (int)    $tokenData->expires_in,
            (string) $tokenData->token_type,
            (string) $tokenData->scope
        );
    }
    
    /**
     * @param Token $token
     * @param int   $reactorIndex
     * @return bool
     * @throws \Deathstar\Exceptions\ClientException
     * @throws \Deathstar\Exceptions\ConnectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function destroyReactor(Token $token, int $reactorIndex): bool
    {
        $response = $this->makeRequest('delete', '/reactor/exhaust/' . $reactorIndex, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token->getAccessToken()
            ]
        ]);
    
        return $response->getStatus() === 204;
    }
    
    /**
     * @param Token  $token
     * @param string $prisoner
     * @return Prisoner
     * @throws \Deathstar\Exceptions\ClientException
     * @throws \Deathstar\Exceptions\ConnectionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws InvalidResponseException
     */
    public function getPrisoner(Token $token, string $prisoner): Prisoner
    {
        $response = $this->makeRequest('get', '/prisoner/' . $prisoner, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token->getAccessToken()
            ]
        ]);
        
        $prisonerData = json_decode($response->getBody());
    
        if (!isset($prisonerData->cell, $prisonerData->block)) {
            throw new InvalidResponseException();
        }
        
        return new Prisoner(
            $prisoner,
            $prisonerData->cell,
            $prisonerData->block
        );
    }
}
