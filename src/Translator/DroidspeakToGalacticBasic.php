<?php

declare(strict_types=1);

namespace Deathstar\Translator;

use Deathstar\Interfaces\DroidspeakTranslator;
use Deathstar\Languages\Droidspeak;
use Deathstar\Utils\Str;

class DroidspeakToGalacticBasic implements DroidspeakTranslator
{
    
    /**
     * @param Droidspeak $droidspeak
     * @return string
     */
    public function translate(Droidspeak $droidspeak): string
    {
        $bytes = str_split(Str::stripWhitespace($droidspeak->getPhrase()), 8);
        $hex = array_reduce($bytes, function($carry, $byte) {
            return $carry .= base_convert($byte, 2, 16);
        });
        
        return pack('H*', $hex);
    }
}
