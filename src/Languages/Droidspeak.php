<?php

declare(strict_types=1);

namespace Deathstar\Languages;

use Deathstar\Exceptions\DroidspeakException;
use Deathstar\Interfaces\Language;

class Droidspeak implements Language
{
    
    /**
     * @var string
     */
    private $phrase;
    
    /**
     * @param string $phrase
     * @throws DroidspeakException
     */
    public function __construct(string $phrase)
    {
        if (!$this->isDroidspeak($phrase)) {
            throw new DroidspeakException("$phrase is not droid speak.");
        }
        
        $this->phrase = $phrase;
    }
    
    /**
     * @param string $phrase
     * @return bool
     */
    private function isDroidspeak(string $phrase): bool
    {
        return preg_match('/^([01]{8}\s?)+$/', $phrase) > 0;
    }
    
    /**
     * @return string
     */
    public function getPhrase(): string
    {
        return $this->phrase;
    }
}
