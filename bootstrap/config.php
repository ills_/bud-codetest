<?php

return [
    'client_id'     => getenv('DEATHSTAR_CLIENT_ID'),
    'client_secret' => getenv('DEATHSTAR_CLIENT_SECRET'),
    'cert_path'     => getenv('DEATHSTAR_CERT_PATH'),
    'key_path'      => getenv('DEATHSTAR_KEY_PATH'),

    \GuzzleHttp\ClientInterface::class => function(\Psr\Container\ContainerInterface $c) {
        return new \GuzzleHttp\Client([
            'base_url' => 'https://death.star.api/',
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Accept' => 'application/json'
            ],
            'verify' => $c->get('cert_path'),
            'ssl_key' => $c->get('key_path')
        ]);
    },
    
    \Deathstar\Client\Client::class => \DI\autowire(\Deathstar\Client\Client::class)
];
