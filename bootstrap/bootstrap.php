<?php

$autoloader = __DIR__ . '/../vendor/autoload.php';

if (!file_exists($autoloader)) {
    echo '---------------------------------------------' . PHP_EOL;
    echo 'PLEASE run `composer install` to get started.' . PHP_EOL;
    echo '---------------------------------------------';
    exit();
}

require_once $autoloader;

$dotenv = \Dotenv\Dotenv::create(__DIR__ . '/../');
$dotenv->load();

$containerBuilder = new \DI\ContainerBuilder();
$containerBuilder->addDefinitions(__DIR__ . '/config.php');
$containerBuilder->useAutowiring(true);
$container = $containerBuilder->build();

return $container;
