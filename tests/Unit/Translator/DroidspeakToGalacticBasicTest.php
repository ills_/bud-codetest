<?php

class DroidspeakToGalacticBasicTest extends \PHPUnit\Framework\TestCase
{
    
    const FOO    = "011001100110111101101111";
    const BAR    = "011000100110000101110010";
    const FOOBAR = "011001100110111101101111011000100110000101110010";
    
    /**
     * @test
     */
    public function it_is_a_translator()
    {
        $this->assertInstanceOf(Deathstar\Interfaces\DroidspeakTranslator::class, new \Deathstar\Translator\DroidspeakToGalacticBasic());
    }
    
    /**
     * @test
     */
    public function it_translates_foo()
    {
        $droidspeak = $this->createMock(Deathstar\Languages\Droidspeak::class);
        $droidspeak->method("getPhrase")->willReturn(self::FOO);
    
        $sut = new Deathstar\Translator\DroidspeakToGalacticBasic();
        
        $this->assertEquals("foo", $sut->translate($droidspeak));
    }
    
    /**
     * @test
     */
    public function it_translates_bar()
    {
        $droidspeak = $this->createMock(Deathstar\Languages\Droidspeak::class);
        $droidspeak->method("getPhrase")->willReturn(self::BAR);
        
        $sut = new Deathstar\Translator\DroidspeakToGalacticBasic();
        
        $this->assertEquals("bar", $sut->translate($droidspeak));
    }
    
    /**
     * @test
     */
    public function it_translates_foo_bar()
    {
        $droidspeak = $this->createMock(Deathstar\Languages\Droidspeak::class);
        $droidspeak->method("getPhrase")->willReturn(self::FOOBAR);
    
        $sut = new Deathstar\Translator\DroidspeakToGalacticBasic();
    
        $this->assertEquals("foobar", $sut->translate($droidspeak));
    }
    
    /**
     * @test
     */
    public function it_translates_binary_with_spaces_in_the_string()
    {
        $foobar = implode(' ', str_split(self::FOOBAR, 8));
    
        $droidspeak = $this->createMock(Deathstar\Languages\Droidspeak::class);
        $droidspeak->method("getPhrase")->willReturn($foobar);
    
        $sut = new Deathstar\Translator\DroidspeakToGalacticBasic();
    
        $this->assertEquals("foobar", $sut->translate($droidspeak));
    }
    
    /**
     * @test
     */
    public function it_translates_cell_2187()
    {
        $bin = '01000011'.
               '01100101'.
               '01101100'.
               '01101100'.
               '00100000'.
               '00110010'.
               '00110001'.
               '00111000'.
               '00110111';
    
        $droidspeak = $this->createMock(Deathstar\Languages\Droidspeak::class);
        $droidspeak->method("getPhrase")->willReturn($bin);
    
        $sut = new Deathstar\Translator\DroidspeakToGalacticBasic();
    
        $this->assertEquals('Cell 2187', $sut->translate($droidspeak));
    }
}
