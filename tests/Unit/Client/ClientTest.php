<?php

class ClientTest extends \PHPUnit\Framework\TestCase
{
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\ConnectionException
     * @expectedExceptionMessage Error Communicating with Server
     */
    public function it_throws_exception_when_error_connecting_to_retrieve_token()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Exception\RequestException(
                "Error Communicating with Server",
                new \GuzzleHttp\Psr7\Request('POST', '/token')
            )
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
    
        $sut = new Deathstar\Client\Client($httpClient);
        $sut->authenticate('client_id', 'client_secret');
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\ClientException
     * @expectedExceptionMessage Invalid Credentials
     */
    public function it_handles_invalid_credentials()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(401, [], \GuzzleHttp\Psr7\stream_for(
                '{
                    "status": 401,
                    "message": "Invalid Credentials"
                }'
            ))
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
    
        $sut = new Deathstar\Client\Client($httpClient);
        $sut->authenticate('client_id', 'client_secret');
    }
    
    /**
     * @test
     */
    public function it_returns_a_valid_token()
    {
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], \GuzzleHttp\Psr7\stream_for(
                '{
                    "access_token": "e31a726c4b90462ccb7619e1b51f3d0068bf8006",
                    "expires_in": "999999999",
                    "token_type": "Bearer",
                    "scope": "TheForce"
                }'
            ))
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new Deathstar\Client\Client($httpClient);
        $token = $sut->authenticate('client_id', 'client_secret');
        
        $this->assertInstanceOf(\Deathstar\Token\Token::class, $token);
        $this->assertEquals('e31a726c4b90462ccb7619e1b51f3d0068bf8006', $token->getAccessToken());
        $this->assertSame(999999999, $token->getExpiresIn());
        $this->assertEquals('Bearer', $token->getTokenType());
        $this->assertEquals('TheForce', $token->getScope());
    }
    
    public function tokenFieldsetProvider()
    {
        return [
            [ "access_token" ],
            [ "expires_in" ],
            [ "token_type" ],
            [ "scope" ]
        ];
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\InvalidResponseException
     * @dataProvider tokenFieldsetProvider
     */
    public function it_throws_exception_if_any_of_the_token_fields_is_missing($field)
    {
        $fieldset = [
           "access_token" => "",
           "expires_in"   => 0,
           "token_type"   => "",
           "scope"        => ""
        ];
        
        unset($fieldset[$field]);

        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], \GuzzleHttp\Psr7\stream_for(json_encode($fieldset)))
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
    
        $sut = new Deathstar\Client\Client($httpClient);
        $sut->authenticate('client_id', 'client_secret');
    }
    
    /**
     * @test
     */
    public function it_destroys_reactor()
    {
        $token = $this->createMock(\Deathstar\Token\Token::class);
        $token->expects($this->once())->method('getAccessToken');
        
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(204)
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new \Deathstar\Client\Client($httpClient);
        $this->assertTrue($sut->destroyReactor($token, 1));
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\ClientException
     */
    public function it_handles_not_found_reactor()
    {
        $token = $this->createMock(\Deathstar\Token\Token::class);
        $token->expects($this->once())->method('getAccessToken');
        
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(404, [], \GuzzleHttp\Psr7\stream_for(
                '{
                    "status": "404",
                    "message": "Reactor with id of 2 not found."
                }'
            ))
        ]);
        
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new \Deathstar\Client\Client($httpClient);
        $sut->destroyReactor($token, 2);
    }
    
    /**
     * @test
     */
    public function it_gets_prisoner()
    {
        $token = $this->createMock(\Deathstar\Token\Token::class);
        $token->expects($this->once())->method('getAccessToken')->willReturn('');
        
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], \GuzzleHttp\Psr7\stream_for(
                '{
                    "cell": "00000000 00000000",
                    "block": "00000000 00000000"
                }'
            ))
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new \Deathstar\Client\Client($httpClient);
        $prisoner = $sut->getPrisoner($token, "prisoner");
        
        $this->assertInstanceOf(\Deathstar\Client\Prisoner::class, $prisoner);
        $this->assertEquals('prisoner', $prisoner->getPrisoner());
        $this->assertEquals('00000000 00000000', $prisoner->getCell());
        $this->assertEquals('00000000 00000000', $prisoner->getBlock());
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\ClientException
     */
    public function it_handles_prisoner_not_found()
    {
        $token = $this->createMock(\Deathstar\Token\Token::class);
        $token->expects($this->once())->method('getAccessToken');
        
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(404, [], \GuzzleHttp\Psr7\stream_for(
                '{
                    "status": "404",
                    "message": "Prisoner not found."
                }'
            ))
        ]);
        
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new \Deathstar\Client\Client($httpClient);
        $sut->getPrisoner($token, 'fake-prisoner');
    }
    
    public function prisonerFieldsetProvider()
    {
        return [
            [ "cell" ],
            [ "block" ]
        ];
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\InvalidResponseException
     * @dataProvider prisonerFieldsetProvider
     */
    public function it_throws_exception_if_any_of_the_prisoner_fields_is_missing($field)
    {
        $fieldset = [
            "cell" => "",
            "block"   => 0
        ];
        
        unset($fieldset[$field]);
    
        $token = $this->createMock(\Deathstar\Token\Token::class);
        $token->expects($this->once())->method('getAccessToken');
        
        $mock = new \GuzzleHttp\Handler\MockHandler([
            new \GuzzleHttp\Psr7\Response(200, [], \GuzzleHttp\Psr7\stream_for(json_encode($fieldset)))
        ]);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $httpClient = new \GuzzleHttp\Client(['handler' => $handler]);
        
        $sut = new Deathstar\Client\Client($httpClient);
        $sut->getPrisoner($token, 'leia');
    }
}
