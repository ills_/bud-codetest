<?php

class StrTest extends \PHPUnit\Framework\TestCase
{
    
    /**
     * @test
     */
    public function it_removes_whitespace_from_beginning_of_string()
    {
        $this->assertEquals('Foo', Deathstar\Utils\Str::stripWhitespace('     Foo'));
    }
    
    /**
     * @test
     */
    public function it_removes_whitespace_from_end_of_string()
    {
        $this->assertEquals('Bar', Deathstar\Utils\Str::stripWhitespace('Bar     '));
    }
    
    /**
     * @test
     */
    public function it_removes_all_whitespace_from_string()
    {
        $this->assertEquals('FooBarBaz', Deathstar\Utils\Str::stripWhitespace(' Foo Bar    Baz '));
    }
    
    /**
     * @test
     */
    public function it_removes_new_lines()
    {
        $this->assertEquals('FooBar', Deathstar\Utils\Str::stripWhitespace("Foo\nBar"));
    }
    
    /**
     * @test
     */
    public function it_removes_carriage_returns()
    {
        $this->assertEquals('FooBar', Deathstar\Utils\Str::stripWhitespace("Foo\rBar"));
    }
}
