<?php

class DroidspeakTest extends \PHPUnit\Framework\TestCase
{
    
    /**
     * @test
     */
    public function it_implements_language_interface()
    {
        $this->assertInstanceOf(\Deathstar\Interfaces\Language::class, new \Deathstar\Languages\Droidspeak('11111111'));
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\DroidspeakException
     * @expectedExceptionMessage FooBar is not droid speak.
     */
    public function it_throws_exception_if_not_binary()
    {
        new \Deathstar\Languages\Droidspeak('FooBar');
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\DroidspeakException
     * @expectedExceptionMessage 11111 is not droid speak.
     */
    public function it_throws_exception_if_phrase_is_less_than_8_characters_long()
    {
        new \Deathstar\Languages\Droidspeak('11111');
    }
    
    /**
     * @test
     */
    public function phrase_can_have_spaces()
    {
        $sut = new \Deathstar\Languages\Droidspeak('11111111 00000000');
        
        $this->assertEquals('11111111 00000000', $sut->getPhrase());
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\DroidspeakException
     * @expectedExceptionMessage 11111111 00000000 foobar is not droid speak.
     */
    public function it_throws_exception_if_binary_has_additional_text()
    {
        new \Deathstar\Languages\Droidspeak('11111111 00000000 foobar');
    }
}
