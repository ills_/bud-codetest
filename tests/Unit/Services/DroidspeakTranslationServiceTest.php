<?php

class DroidspeakTranslationServiceTest extends \PHPUnit\Framework\TestCase
{
    
    /**
     * @test
     */
    public function it_translates_prisoner_location_to_galactic_basic()
    {
        $prisoner = $this->createMock(\Deathstar\Client\Prisoner::class);
        $prisoner->expects($this->once())->method('getPrisoner')->willReturn('Leia');
        $prisoner->expects($this->once())->method('getCell')->willReturn('01000011 01100101 01101100 01101100 00100000 00110010 00110001 00111000 00110111');
        $prisoner->expects($this->once())->method('getBlock')->willReturn('01000100 01100101 01110100 01100101 01101110 01110100 01101001 01101111 01101110'
                                                                                         .'00100000 01000010 01101100 01101111 01100011 01101011 00100000 01000001 01000001'
                                                                                         .'00101101 00110010 00110011 00101100');
        
        $translator = new \Deathstar\Translator\DroidspeakToGalacticBasic();
        $sut = new \Deathstar\Services\DroidspeakTranslationService($translator);
        $result = $sut->translatePrisonerLocation($prisoner);
        
        $this->assertInstanceOf(\Deathstar\Client\Prisoner::class, $result);
        $this->assertEquals('Cell 2187', $result->getCell());
        $this->assertEquals('Detention Block AA-23,', $result->getBlock());
    }
    
    /**
     * @test
     * @expectedException \Deathstar\Exceptions\PrisonerTranslationException
     * @expectedExceptionMessage Could not translate prisoner location.
     */
    public function it_throws_exception_if_prisoner_could_not_be_translated()
    {
        $prisoner = $this->createMock(\Deathstar\Client\Prisoner::class);
        $prisoner->expects($this->once())->method('getPrisoner')->willReturn('Leia');
        $prisoner->expects($this->once())->method('getCell')->willReturn('FooBarBaz');
    
        $translator = new \Deathstar\Translator\DroidspeakToGalacticBasic();
        $sut = new \Deathstar\Services\DroidspeakTranslationService($translator);
        $sut->translatePrisonerLocation($prisoner);
    }
}
